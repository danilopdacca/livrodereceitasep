+++
title = 'Risotto de Funghi'
date = 2023-09-28T09:10:53-03:00
draft = false

ingredientes = [
    "2 xícaras (chá) de arroz para risotto",
    "¾ xícara (chá) de funghi seco (40 g)",
    "1,5 litro de caldo de legumes",
    "½ cebola",
    "1 dente de alho",
    "½ xícara (chá) de vinho branco",
    "4 colheres (sopa) de manteiga",
    "⅓ de xícara (chá) de queijo parmesão ralado",
    "4 ramos de salsinha",
    "sal e pimenta-do-reino moída na hora a gosto"
]


descric = "Cremoso, saboroso e simplesmente incrível, este risotto de funghi é uma delícia pura. A mistura perfeita de textura suave com o gosto rico dos cogumelos funghi. NÃO TEM ERRO. SÓ FAZ!"
image = "../../images/risoto.jpeg"
image2= "../images/risoto.jpeg"
+++

## Modo de preparo:

1. Numa tigela coloque o funghi, cubra com 1 xícara (chá) de água fervente e deixe hidratar por alguns minutos enquanto separa os outros ingredientes.
2. Descasque e pique fino a cebola e o alho. Lave, seque e pique fino a salsinha.
3. Sobre a jarra medidora, escorra o funghi hidratado numa peneira. Pressione delicadamente com a colher para extrair todo o líquido. Acrescente o caldo de legumes ao líquido do funghi até completar 1,5 litro – ele vai ser usado para o cozimento do risotto. Transfira os cogumelos para a tábua e corte em pedaços médios.
4. Coloque 2 colheres (sopa) de manteiga numa panela média e leve ao fogo médio. Quando derreter, adicione a cebola, tempere com uma pitada de sal e refogue por 4 minutos até murchar. Acrescente o funghi e o alho e mexa por 1 minuto. Junte o arroz e refogue por 2 minutos para envolver os grãos com a manteiga. Tempere com sal e pimenta a gosto – lembre-se que o caldo caseiro não leva sal.
5. Regue com o vinho e mexa bem até secar. Adicione 2 conchas do caldo e misture bem. Deixe cozinhar, mexendo de vez em quando, até secar. Repita o procedimento, adicionando o caldo, de concha em concha, mexendo, até o risotto ficar no ponto – o grão deve estar cozido mas ainda durinho no centro (al dente).

6. Atenção: na última adição de caldo, mexa e desligue o fogo. Não deixe secar completamente, o risotto deve ficar bem úmido. Acrescente a manteiga restante, o parmesão e misture bem. Sirva a seguir com a salsinha picada.

## O Segredo de um Bom Risotto

O segredo está no Passo 5: simplesmente em ir adicionando aos poucos o caldo de legumes no risotto e NÃO PARAR DE MEXER. Quando der uma leve secada, adiciona mais caldo e continua mexendo.
## PS

(A imagem desse risotto lá em cima é meramente ilustrativa. O risotto presente nessa foto também é de Funghi, mas não foi feita por mim. Ela é de um restaurante chamado Urban Kitchen, mas o risotto dessa receita fica com a mesma cara).
