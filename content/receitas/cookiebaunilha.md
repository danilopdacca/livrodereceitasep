+++
title = 'Cookies de Chocolate'
date = 2023-09-28T09:11:08-03:00
draft = false
ingredientes = [
    "½ xícara de açúcar",
    "¾ xícara de açúcar mascavo",
    "1 colher de chá de sal",
    "½ xícara de manteiga derretida",
    "1 ovo",
    "1 colher de chá de extrato de baunilha",
    "1 ¼ xícara de farinha de trigo",
    "½ colher de chá de bicarbonato de sódio",
    "110g de pedaços de chocolate ao leite ou meio amargo",
    "110g de pedaços de chocolate escuro (ou de sua preferência)"
]
descric = "Essa receita é aquele típico clássico que todos gostam e que dá pra comer qualquer hora do dia. Cookie bem macio, que derrete na boca. Bom de comer na temperatura ambiente e melhor ainda quentinho."
image = "../../images/photo-1583743089695-4b816a340f82.jpeg"
image2= "../images/photo-1583743089695-4b816a340f82.jpeg"
+++

## Modo de Preparo

1. Preaqueça o forno a 180º C.
2. Em uma tigela grande, bata junto o açúcar normal e mascavo, sal e a manteiga derretida até formar uma pasta sem nenhum pedacinho.
3. Bata o ovo e a baunilha até que fios leves escorram do batedor.
4. Peneire a farinha e o bicarbonato, e em seguida dobre a mistura com uma espátula, tomando cuidado para não misturar demais. Isso fará com que o glúten na farinha endureça, resultando em cookies com sabor mais caseiro.
5. Misture os pedaços de chocolate de forma uniforme, e em seguida deixe a massa descansar por aproximadamente 30 minutos. Para um sabor de caramelo mais intenso e uma cor mais forte, deixe a massa descansando de um dia para o outro. Quanto mais a massa descansar, mais complexo será seu sabor.
6. Retire a massa com uma colher de sorvete e coloque em uma assadeira forrada com papel manteiga, deixando pelo menos 10 centímetros de espaço entre os cookies e 5 centímetros da beirada da assadeira para que os cookies possam se distribuir de maneira uniforme.

   (Dica da Bia: para eles ficarem mais altinhos, coloca a massa já em bolinhas na geladeira.)

7. Asse por entre 12 e 15 minutos, ou até que as bordas estejam quase começando a dourar.
8. Deixe esfriar completamente (ou façam como eu e já podem comer assim que sair do forno), e saboreie!     

### (Dica pro dia seguinte: Se sobrarem cookies pra outro dia vale muito a pena colocar uns segundos no micro-ondas. Vai parecer que acabou de sair do forno)